import { RECUPERO_USUARIO, SET_MESSAGE,METODOS_VALIDOS,METODOS_VALIDOS_INVOCADO,RECUPERO_SUCCESS,SUCCESSPASS } from "../types"
import { ChangePass, retriveUser, validarTOken, olvidoBloqueo, ValidadoresUsuarios,ValidadoresInvoca,ChangePassOlvido,ValidadoresInvocaTCO,ValidadoresTCO } from "./../../services/changePass";

export const recuperoClaveExpirada = (passOld, passNew) => (dispatch) => {

    return ChangePass(passOld,passNew).then(
        ({ data }) => {
            if (data.estado === "0") {
                dispatch({
                    type: SUCCESSPASS,
                    payload: { user: data },
                }); 
            } else {
                dispatch({
                    type: 'CLAVE_INVALIDA'
                });
            }
            return Promise.reject();
        }
    );
}

export const retrieveUser = (dni,captcha) => (dispatch) => {
    return retriveUser(dni,captcha).then(
        ({ data }) => {
            if (data.estado === "0") {
                dispatch({
                    type: RECUPERO_USUARIO,
                    payload: { user: data.datos },
                });
            } else {
                dispatch({
                    type: SET_MESSAGE,
                    payload: data,
                })
            }
        }
    ).catch(e => console.log(e.message));
}



export const olvidoBloqueoUser = (dniUser, User, captcha) => (dispatch) => {

    return   olvidoBloqueo(dniUser, User,captcha).then(
        ({ data }) => {
            if (data.estado === "0") {
                     ValidadoresUsuarios(dniUser).then(
                            ({ data }) => {
                     if (data.estado === "0") {
                                        dispatch({
                                            type: METODOS_VALIDOS,
                                            payload: { user: data.validadores },
                                        });
                                    }
                                })
        }}
            ).catch(e => 
                dispatch({
                    type: SET_MESSAGE,
                    payload: e.message,
                })
                );
}


export const validadorLlamado = (validador)=> (dispatch) => {
    if (validador === "TARJETA_COORDENADAS")
    {
        ValidadoresInvocaTCO().then(
            ({ data }) => {
                        console.log(data)
                            if (data.estado === "0") {
                                dispatch({
                                    type: METODOS_VALIDOS_INVOCADO,
                                    payload: {respuestaValidador:data.datos},
                                });
                            } else {
                                dispatch({
                                    type: SET_MESSAGE,
                                    payload: data,
                                })
                            }
                        }
        )

    }else
    {
        ValidadoresInvoca(validador).then(
            ({ data }) => {
                        console.log(data)
                            if (data.estado === "0") {
                                dispatch({
                                    type: METODOS_VALIDOS_INVOCADO,
                                    payload: { respuestaValidador: "0" },
                                });
                            } else {
                                dispatch({
                                    type: SET_MESSAGE,
                                    payload: data,
                                })
                            }
                        }
        )
    }
    
}


export const validarToken = (tokenUser,codigo)=> (dispatch) => {
    validarTOken(tokenUser,codigo).then(
        ({ data }) => {
            console.log(data)
                        if (data.estado === "0") {
                            dispatch({
                                type: RECUPERO_SUCCESS,
                            });
                        } else {
                            dispatch({
                                type: SET_MESSAGE,
                                payload: data,
                            })
                        }
                    }
    )
}



export const recuperoClaveOlvido = (passNew) =>(dispatch) => {
    return ChangePassOlvido (passNew).then(
        ({ data }) => {
            if (data.estado === "0") {
                dispatch({
                    type: SUCCESSPASS,
                    payload: { user: data },
                }); 
            } else {
                dispatch({
                    type: 'CLAVE_INVALIDA'
                });
            }
            return Promise.reject();
        }
    );
}


export const recuperoUserSuccess = () => (dispatch) => {
    dispatch({
        type: RECUPERO_SUCCESS,
    });
}



export const validarTokenTCO = (t1,t2)=> (dispatch) => {
    ValidadoresTCO(t1,t2).then(
        ({ data }) => {
                        if (data.estado === "0") {
                            dispatch({
                                type: RECUPERO_SUCCESS,
                            });
                        } else {
                            dispatch({
                                type: SET_MESSAGE,
                                payload: data,
                            })
                        }
                    }
    )
}
