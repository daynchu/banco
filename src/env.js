let BASE_URL ;

if (process.env.REACT_APP_BACKEND_URL === "DESA") {
    BASE_URL = process.env.REACT_APP_URL_DESA
} else if (process.env.REACT_APP_BACKEND_URL === "INTE") {
    BASE_URL = process.env.REACT_APP_URL_INTE
} else if (process.env.REACT_APP_BACKEND_URL === "HOMO") {
    BASE_URL = process.env.REACT_APP_URL_HOMO
} else if (process.env.REACT_APP_BACKEND_URL === "Production") {
    BASE_URL = process.env.REACT_APP_URL_PROD
} else 
{
  if (BASE_URL === "" )
  {
    console.log(process.env.REACT_APP_BACKEND_URL);
  }
  BASE_URL = "http://localhost:8080";
  
}


const ENV = {
  BASE_URL
//   API: {
//     HEADERS: {
//       'Content-Type': 'application/json',
//       Accept: 'application/json',
//     },
//     LOGIN: `${BASE_URL}/v2/login`,
//     },
};

export default ENV;
