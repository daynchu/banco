import axios from "axios"

// export const getMetodoSinLogueo = (url,parametros,loadingScreen,callback) => {
    
// }


export const getMetodo = (url,parametros,loadingScreen) => {
    return axios.get(url, parametros,{ withCredentials: true });
}

export const postMetodo = (url,parametros,loadingScreen) => {
    return axios.post(url, parametros,{ withCredentials: true });
}

export const PutMetodo = (url,parametros,loadingScreen,callback) => {
    return axios.put(url, parametros,{ withCredentials: true });
}


// export const postPutMetodoConLogueo = (url,parametros,loadingScreen,callback) => {
    
// }