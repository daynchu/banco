import axios from "axios";
import ENV from "../env";

export const ChangePass = (passOld,PassNew) => {
    // return axios.post("http://backend-unificado-canales-desa.appd.bh.com.ar/ob/api/cambiar-clave", {
    return axios.post(ENV.BASE_URL + "/ob/api/cambiar-clave", {
      clave: PassNew ,
      claveAnterior:passOld
    },{ withCredentials: true });
  };

export const retriveUser = (dni,captcha) => {
  // return axios.post("http://backend-unificado-canales-desa.appd.bh.com.ar/ob/api/recuperar-usuario", {
  return axios.post(ENV.BASE_URL + "/ob/api/recuperar-usuario", {
    numeroDocumento:dni,
    captcha:captcha
  })
}

export const ChangePassOlvido = (newPass) => {
  return axios.post(ENV.BASE_URL + "/ob/api/recuperar-clave", {
    clave: newPass ,
  },{ withCredentials: true });
}

// export const validarCaptcha = (captchaUser) =>{
// return axios.post("http://backend-unificado-canales-desa.appd.bh.com.ar/ob/api/validar-captcha",{
//   captcha:captchaUser
// })
// }

export const olvidoBloqueo = (dniUser,user,captchaUser) => {
  // return axios.post("http://backend-unificado-canales-desa.appd.bh.com.ar/ob/api/pseudo-login",{
  return axios.post(ENV.BASE_URL + "/ob/api/pseudo-login",{
    numeroDocumento:dniUser,
    usuario:user,
    captcha:captchaUser
  },{ withCredentials: true })
}

export const ValidadoresUsuarios = (dniUser) => {
  // return axios.post ("http://backend-unificado-canales-desa.appd.bh.com.ar/ob/api/validadores",{
  return axios.post (ENV.BASE_URL + "/ob/api/validadores",{
    numeroDocumento:dniUser
  },{ withCredentials: true })
}

export const ValidadoresInvoca = (valida) => {
  // return axios.post ("http://backend-unificado-canales-desa.appd.bh.com.ar/ob/api/enviar-token",{
  return axios.post (ENV.BASE_URL + "/ob/api/enviar-token",{
    validador:valida
  },{ withCredentials: true })
}


export const validarTOken = (tokenUser,codigo) => {
  // return axios.post ("http://backend-unificado-canales-desa.appd.bh.com.ar/ob/api/validar-token",{
  return axios.post (ENV.BASE_URL + "/ob/api/validar-token",{
    token:tokenUser,
    validador:codigo
  },{ withCredentials: true })
}

export const ValidadoresInvocaTCO = (t1,t2) => {
  return axios.post (ENV.BASE_URL + "/ob/api/desafio-tco",{
    respuesta1:t1,
    respuesta2:t2
  },{ withCredentials: true })
}


export const ValidadoresTCO = (t1,t2) => {
  return axios.post (ENV.BASE_URL + "/ob/api/validar-tco",{
    respuesta1:t1,
    respuesta2:t2
  },{ withCredentials: true })
}