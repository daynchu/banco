import React from "react";
import './sideBar.scss'
import Close from'./../../../assets/iconos/close.svg'
import { useDispatch } from "react-redux";
import { logout } from './../../../actions/auth';
import FAvanzar from "../../../assets/iconos/flecha_avanzar.svg"
import { Link } from "react-router-dom";



const menuArray= [
  {
    id:1,
    name: "Inicio",
    // icon: "handMoney",
    link: '/home',
  },
  {
    id:2,
    name: "Cuentas",
    // icon: "product",
    link: '/Cuenta',
  },
  {
    id:3,
    name: "Pagos",
    // icon: "fileEdit",
    link: '/Pago',
  },
  {
    id:4,
    name: "Transferencia",
    // icon: "fileSetting",
    link: '/Transferencia',
  },
  {
    id:5,
    name: "Inversiones",
    // icon: "userSetting",
    link: '/Inversiones',
  },
  {
    id:6,
    name: "Cobros",
    // icon: "building",
    link: '/Cobros'
  },
  {
    id:7,
    // title: "Cheques",
    name: "Cheques",
    // icon: "multipleUser",
    link: '/cheques'
  },
  {
    id:7,
    // title: "Cheques",
    name: "Administracion",
    // icon: "multipleUser",
    link: '/Administracion'
  },
]


export default function SideBar ({open, close}){

  const dispatch = useDispatch();
    const desloguear =() =>{
        dispatch(logout())
    }

  return (
      
      <div>
        <div>
          <div id={(open ? 'menu-botones' : 'menu-botonesMob')}>
                <div id="btnCloseMenu" onClick={() => close()}>
                  <img id="iconClose" src={Close} alt="close" />
                </div>
                <div id="perfilSideBar">
                  <div>
                <svg height="64" width="64">
              <circle cx="32" cy="32" r="32" fill="#06CA75"/>
              <text font-size="32" font-family="Arial, Helvetica, sans-serif" fill="white" text-anchor="middle" x="32" y="43"
              >MG</text>
                </svg>
                </div>
                <div>
                  <h1 style={{color:"#343232"}}>Hola,Mía</h1>
                  <h3 style={{color:"#805ADC"}} onClick={() =>{console.log("hola")}}>ver mi perfil <img src={FAvanzar} alt="flecha" style={{marginBottom:"-2px"}}  /> </h3>
                </div>
                </div>

              {menuArray.map ((ItemMenu) =>
               <Link to={ItemMenu.link}>
                <button id="invisible" >{ItemMenu.name}</button>
               </Link>
                          )}

                {/* <Link to="/home">
                <button id="invisible" >Inicio</button>
                </Link>
                <button id="invisible" >Cuentas</button>
                <button id="invisible" >Pagos</button>
                <button id="invisible" >Transferencias</button>
                <button id="invisible" >Inversiones</button>
                <button id="invisible" >Cobros</button>
                <button id="invisible" >Cheques</button>
                <button id="invisible" >Administración</button> */}
                
                <button id="invisible" type="button" onClick={() => {desloguear()}} style={{fontWeight: 'bold'}}>Cerrar Sesion</button >         </div>
        </div>
      </div>    
  );
};