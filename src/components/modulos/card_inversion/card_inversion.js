import React,{useState} from 'react'
import './card_inversion.scss'
import FAvanzar from "../../../assets/iconos/flecha_avanzar.svg"
import { Link } from "react-router-dom";
import FormatAmount from '../../../formatAmount'

export default function CardInversiones() {

    const [cInversiones]=useState([
        {id: 1, monto:"150000.00",fecha:"15/08/2021", tipo: "Plazo Fijo"},
        {id: 2, monto:"150000.00",fecha:"15/08/2021", tipo: "Plazo Fijo"},
    ])

    return (
        <div>
            <div id="headerHInversion">
                <div><h3>Inversiones</h3></div>
                <div id="flechaHInversion">
                    <Link to="/error"> 
                        <img src={FAvanzar} alt="flecha" />
                    </Link>
                </div>
            </div>
            {cInversiones.map((cInversion,index) => 
            <div key={cInversion.id}>
                <div id="containerHInversion">
                    <div id="dataHInversiones">
                        <div>$ <FormatAmount number={cInversion.monto} /></div>
                        <div id="dataFecha">Vence {cInversion.fecha}</div>
                    </div>    
                    <div id="dataTipo">
                        {cInversion.tipo}
                    </div>         
                </div>
            </div>            
            )}
        </div>
    );   
}