import React from "react";
import './headerHomeMobile.scss'
import menu from './../../../assets/iconos/menu.svg'
import notification from './../../../assets/iconos/notification.svg'
import user from './../../../assets/iconos/user.svg'

export default function HeaderComponentMobile ({open}) {
  
  return (
    <div>
        <div id="containerMenu">
            <div>
                <div>
                    <img id="iconMenu" src={menu} alt="menu" />
                </div> 
            </div>
            <div>
                <div>
                    <img id="iconMenu" src={notification} alt="menu" />  
                </div>
            </div>
            <div>
                <div>
                    <img id="iconMenu" src={user} alt="menu" />  
                </div>
            </div>
        </div>
    </div>
  );
}