import React from 'react'
import './card_link.scss'
import {Link} from 'react-router-dom'
import haberes from "../../../assets/iconos/haberes.svg"
import transferir from "../../../assets/iconos/transferir.svg"
import plus from "../../../assets/iconos/plus.svg"

export default function CardLink() {
    return (
        <div>
            <div>
                <div id="containerHLink">
                    <div id="iconLink">
                    <div>
                        <Link to={location => ({ ...location, pathname: "/error" })} >
                            <label>
                                <div><img id="iconImg" src={transferir} alt="flechas" /></div>
                                <button id="buttonIcon">Transferencias</button>
                            </label>
                        </Link>
                        </div>
                    </div>
                    <div id="iconLink">
                        <div >
                        <Link to={location => ({ ...location, pathname: "/error" })} >
                            <label>
                                <div><img id="iconImg" src={haberes} alt="haberes" /></div>
                                <button id="buttonIcon">Pago Haberes</button>
                            </label>
                        </Link>
                        </div>
                    </div>
                    <div id="iconLink">
                    <div >
                        <Link to={location => ({ ...location, pathname: "/error" })} >
                            <label>
                                <div><img id="iconImg" src={plus} alt="plus" /></div>
                                <button id="buttonIcon" >Más Acciones</button>
                            </label>
                        </Link>
                        </div>
                    </div>
                </div>              
            </div>
        </div> 
    );   
}