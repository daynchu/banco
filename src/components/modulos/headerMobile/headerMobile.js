import React from "react";
import './headerMobile.scss'
import { Link } from "react-router-dom";
import arrow from './../../../assets/iconos/Back.svg';

export default function HeaderComponentMobile ({ path }) {
 
  return (
    <div className="posicion_headerMobile">
     <div className="headerMobile">
        <div id="back">
          <Link to={{pathname: path}}>
            <img src={arrow} className="githubIcon" alt="back" />
          </Link>
        </div>
      </div>
      </div>
  );
}