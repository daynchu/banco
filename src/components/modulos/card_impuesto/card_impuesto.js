import React,{useState} from 'react'
import './card_impuesto.scss'
import FAvanzar from "../../../assets/iconos/flecha_avanzar.svg"
import { Link } from "react-router-dom";
import FormatAmount from '../../../formatAmount'

export default function CardImpuesto() {

    const [cImpuestos]=useState([
        {id: 1, item:"Edenor",monto:"15000.00", fecha: "15/08/2021"},
        {id: 2, item:"AySA",monto:"5000.00", fecha: "15/08/2021"},
    ])

    return (
        <div>
            <div id="headerHImpuesto">
                <div><h3>Impuestos y Servicios</h3></div>
                <div id="flechHaImpuesto">
                    <Link to="/error"> 
                        <img src={FAvanzar} alt="flecha" />
                    </Link>
                </div>
            </div>
            {cImpuestos.map((cImpuesto,index) => 
            <div key={cImpuesto.id}>
                <div id="containerHImpuesto">
                    <div id="dataHImpuestos">
                        <div>{cImpuesto.item}</div>
                        <div id="dataMontoImpuesto">$ <FormatAmount number={cImpuesto.monto} /></div>
                    </div>    
                    <div id="dataFechaImpuesto">
                        Vence {cImpuesto.fecha}
                    </div>         
                </div>
            </div>            
            )}
        </div>
    );   
}