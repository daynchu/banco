import React,{useState} from 'react'
import './card_cuenta.scss'
import FAvanzar from "../../../assets/iconos/flecha_avanzar.svg"
import FgAvanzar from "../../../assets/iconos/arrow-green.svg"
import { Link } from "react-router-dom";
import FormatAmount from '../../../formatAmount'

export default function CardCuenta() {

    const [cCuentas]=useState([
        {id: 1, nro:"1234",monto:"1390789.67"},
        {id: 2, nro:"5678",monto:"489354.76"},
    ])

    return (
        <div>
            {cCuentas.map((cCuenta,index) => 
            <div key={cCuenta.id}>
                <div id="containerHCuenta">
                    <div id="headerHCuenta">
                        <h5>Cuenta XXXX-{cCuenta.nro}</h5>
                    </div>
                    <div id="dataHCuenta">
                        <div><h1>$ <FormatAmount number={cCuenta.monto} /></h1></div>
                        <div id="flechaHCuenta">
                            <Link to="/error"> 
                                <img src={FAvanzar} alt="flecha" />
                            </Link>
                        </div>
                    </div>                
                </div>
            </div>            
            )}
            <div id="footerHCuenta">
                <div id="dataHCuenta">
                    <div><h3 id="btnCuentas" >Ver más cuentas</h3></div>
                    <div id="flechaHCuenta">
                        <Link to="/error"> 
                            <img src={FgAvanzar} alt="flecha" />
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );   
}