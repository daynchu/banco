import React,{useState} from 'react'
import './card_movimiento.scss'
import FgAvanzar from "../../../assets/iconos/arrow-green.svg"
import { Link } from "react-router-dom";
import FormatAmount from '../../../formatAmount'

export default function CardMovimiento() {

    const [cMovimientos]=useState([
        {id: 1, item:"Transferencia masiva",monto:"-150000.00", cuenta:"1234"},
        {id: 2, item:"Pago de háberes",monto:"-150000.00", cuenta:"1234"},
    ])

    const [cAyerMovimientos]=useState([
        {id: 1, item:"Pago Edenor",monto:"-150000.00", cuenta:"1234"},
        {id: 2, item:"Acreditación Plazo Fijo",monto:"150000.00", cuenta:"1234"},
        {id: 3, item:"Transferencia masiva",monto:"-150000.00", cuenta:"1234"},
        {id: 4, item:"Transferencia masiva",monto:"-150000.00", cuenta:"1234"},
    ])

    return (
        <div>
            <div>
                <div>
                    <div>
                        <h3>Últimos movimientos</h3>
                    </div>                    
                    <div>
                        <div id="titleHMovimientos">
                            <h5>Hoy</h5>
                        </div>
                        {cMovimientos.map((cMovimiento,index) =>
                        <div id="containerHMovimientos" key={cMovimiento.id}>
                            <div id="dataHMovimiento">
                                <div>{cMovimiento.item}</div>
                                <div id="dataMontoMovimiento"
                                    className={(cMovimiento.monto > 0) ? 'positiveMovimiento' : ''}>
                                    $ <FormatAmount number={cMovimiento.monto} />                                        
                                </div>
                            </div>    
                            <div id="dataCuentaMovimiento">
                                Cuenta XXXX-{cMovimiento.cuenta}
                            </div>         
                        </div>
                        )}
                    </div>     
                    <div id="movimientoMob">
                        <div id="titleHMovimientos"><h5>Ayer</h5></div>
                        {cAyerMovimientos.map((cAyerMovimiento,index) =>
                        <div id="containerHMovimientos" key={cAyerMovimiento.id}>
                            <div id="dataHMovimiento">
                                <div>{cAyerMovimiento.item}</div>
                                <div id="dataMontoMovimiento" 
                                    className={(cAyerMovimiento.monto > 0) ? 'positiveMovimiento' : ''}>
                                    $ <FormatAmount number={cAyerMovimiento.monto} />
                                </div>
                            </div>    
                            <div id="dataCuentaMovimiento">
                                Cuenta XXXX-{cAyerMovimiento.cuenta}
                            </div>         
                        </div>
                        )}
                    </div>     
                    <div>
                        <div id="dataHMovimiento">
                            <div><h3 id="btnMovimiento" >Ver más</h3></div>
                            <div id="flechaHMovimiento">
                                <Link to="/error"> 
                                    <img src={FgAvanzar} alt="flecha" />
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    );   
}