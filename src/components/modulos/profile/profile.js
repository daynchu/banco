import React from "react";
import './profile.scss'
import imgProfile from '../../../assets/iconos/profile.svg'
import ADown from '../../../assets/iconos/arrow-down.svg'

export default function Profile () {
  
  return (
    <div>
        <div className="menu-trigger">
            <img id="iconProfile" src={imgProfile} alt="profile" />
            <div id="userName">Red S.A.</div>
            <img id="iconDropDown" src={ADown} alt="profile" />
        </div>
    </div>
  );
}