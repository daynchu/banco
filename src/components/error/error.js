import './error.scss'
import React  from 'react';
import HeaderComponent from "./../modulos/header/header"

export default function Error() {

    return(
        <div>
            <div>
                <div id="header_desktop">
                    <HeaderComponent />
                </div>
                <div id="containerError">            
                    <div>
                        <div style={{backgroundColor: "gray", height:44, width:44, marginLeft:74}}></div>
                    </div>    
                    <div>
                        <h3>Se ha procudido un error</h3>
                    </div>                
                    <div>
                        <h4>Vuelvalo a intentarlo más tarde</h4>
                    </div>                
                </div>
            </div>
        </div>
    )     
}