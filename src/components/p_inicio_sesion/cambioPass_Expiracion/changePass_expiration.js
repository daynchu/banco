import React,{useState} from 'react';
import HeaderComponent from "../../modulos/header/header";
import HeaderMobile from '../../modulos/headerMobile/headerMobile'
import "./changePass_expiration.scss"
import eye_open from "../../../assets/iconos/eye_open.svg"
import eye_close from "../../../assets/iconos/eye_close.svg"
import { Link,Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { recuperoClaveExpirada } from "../../../actions/validaciones/recupero";

export default function ChangePassExpiration () {
    const [tipo, setTipo] = useState(false);
    const [borderError, setBorderError] = useState(false);
    const [passOldDistintos, setpassOldDistintos] = useState(false);
    const [passNewDistintos, setpassNewDistintos] = useState(false);
    const [DatosUsuario,SetDatosUsuarios] = useState({
        passOld:'',
        pass1: '',
        pass2: ''
    })

    const dispatch = useDispatch();
    const data = useSelector((state) => state.auth);

    const handleChange=(e)=>{
        SetDatosUsuarios({
            ...DatosUsuario,
            [e.target.name] : e.target.value
        })
          
    }

    const ocultarContraseña = () => {
        setTipo(tipo ? false : true);
    }   

    const validarContraseña = () => {

        if(DatosUsuario.passOld === data.validaciones.contaseña)
        {

        if(DatosUsuario.pass1 === DatosUsuario.pass2 )
        {
            if( DatosUsuario.passOld !== '' && DatosUsuario.pass1 !== '' && DatosUsuario.pass2 !== ''){
                dispatch(recuperoClaveExpirada(DatosUsuario.passOld,DatosUsuario.pass1))
                setBorderError(false);
            }else{
                setBorderError(true);
            }
        }else{
            setBorderError(true);
            setpassNewDistintos(true)
            setpassOldDistintos(false)           
        }
        
    }else{
        setBorderError(true);
        setpassOldDistintos(true)
    }
}


    return(
        <div>
            {data.validaciones === null ? <Redirect to={{pathname: '/login'}}/> : 
            
            <div>
            {data.user !== null ? <Redirect to={{pathname: '/NuevaClaveSucces'}}/>   :
            <div>
            <div id="header_desktop">
                <HeaderComponent />
            </div>
            <div id="header_mobile">
                <HeaderMobile path={'/login'}/>
            </div>
            <div id="containerFormChangePass">            
                <div className="containerforRusuarioGrid">
                    <h1>Creá tu nueva clave</h1>
                </div>    
                <div id="inputPosition" >
                    <input 
                    type={tipo ? "text" : "password"} name="passOld" onChange={handleChange} 
                    id={(borderError ? 'error' : 'inputBlancoVctoPass')}
                    placeholder="Tu clave actual" />
                    <label onClick={() => {ocultarContraseña()}}>
                    {tipo ? <img src={eye_open} alt="ojo" id="inputImg"/> : <img src={eye_close} alt="ojo" id="inputImg"/> }
                    </label>
                    {passOldDistintos ?  <h5 style={{marginTop:"0em",color:"red"}}>Los datos ingresados son incorrectos</h5> : <h5></h5>}
                </div>    
                <div id="inputPosition" >
                    <input 
                    type={tipo ? "text" : "password"} name="pass1" onChange={handleChange} 
                    id={(borderError ? 'error' : 'inputBlancoVctoPass')}
                    placeholder="Nueva clave" />
                    <label onClick={() => {ocultarContraseña()}}>
                    {tipo ? <img src={eye_open} alt="ojo" id="inputImg"/> : <img src={eye_close} alt="ojo" id="inputImg"/> }
                    </label>
                </div>            
                <div id="inputPosition">
                    <input 
                    type={tipo ? "text" : "password"} name="pass2" onChange={handleChange} 
                    id={(borderError ? 'error' : 'inputBlancoVctoPass')}
                    placeholder="Repetir Nueva clave" /> 
                    <label onClick={() => {ocultarContraseña()}}>
                    {tipo ? <img src={eye_open} alt="ojo" id="inputImg"/> : <img src={eye_close} alt="ojo" id="inputImg"/> }
                    </label> 
                    {passNewDistintos ? <h5 style={{marginTop:"0em",color:"red"}}>Las claves no coinciden</h5>: <h5></h5>}                   
                </div>   
                <div>
                    <h4 id="containerText">
                        La clave debe ser de 8 a 14 caracteres.<br/>
                        Debe contener letras, números y al menos un caracter en mayúscula.
                    </h4>
                </div>
                <div id="containerformBTNVctoPass">
                    <div>
                        <Link to="/Login" replace >
                            <button id="buttonWhite">Cancelar</button>
                        </Link>                    
                    </div>
                    <div>
                        <button 
                            id="buttonGreen"
                            onClick={() => {validarContraseña()}}>
                            Continuar
                        </button>
                    </div>
                </div>    
            </div>

            </div>
            }
</div>

            
            }
            
        </div>
    )
}