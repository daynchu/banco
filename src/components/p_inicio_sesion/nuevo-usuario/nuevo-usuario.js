import React,{useState} from 'react';
import HeaderComponent from "../../modulos/header/header";
import "./nuevo-usuario.scss"
import { Link,Redirect } from 'react-router-dom';

export default function ChangePassBlock () {

    return(
        <div>
            <div>
            <div id="header_desktop">
                <HeaderComponent />
            </div>
            <div id="containerNewUser">            
                <div className="containerforRusuarioGrid">
                    <h1>Creá tu nueva usuario</h1>
                </div>    
                <div id="inputPositionNewUser" >
                    <input placeholder="DNI" id="inputBlancoNuevoUsuario" />
                </div> 
                <div id="inputPositionNewUser" >
                    <input placeholder="Nueva Usuario" id="inputBlancoNuevoUsuario" />
                </div>            
                <div id="inputPosition">
                    <input placeholder="Repetir Nueva Usuario" id="inputBlancoNuevoUsuario"/>                   
                </div>  
                <div id="inputPositionNewUser" >
                    <input placeholder="Nueva clave" id="inputBlancoNuevoUsuario" />
                </div>            
                <div id="inputPosition">
                    <input placeholder="Repetir Nueva clave" id="inputBlancoNuevoUsuario"/>                   
                </div>  
                <div id="containerFormNewUser">
                    <div className="btnCancelMob">
                        <Link to="/Login" replace >
                            <button id="buttonWhite">Cancelar</button>
                        </Link>                    
                    </div>
                    <div>
                        <button 
                            id="buttonGreen" className="btnContinueMob">
                            Continuar
                        </button>
                    </div>
                </div>    
            </div>
            </div>
            
        </div>
    )
}