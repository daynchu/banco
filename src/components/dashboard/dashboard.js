import './dashboard.scss'
import React, {useState} from 'react'
/* import { useDispatch } from "react-redux";
import { logout } from "../../actions/auth"; */
import HeaderComponent from "./../modulos/header/header";
import HeaderHomeMobile from "./../modulos/headerHomeMobile/headerHomeMobile";
import CardCuenta from '../modulos/card_cuenta/card_cuenta';
import CardMovimiento from '../modulos/card_movimiento/card_movimiento';
import CardLink from '../modulos/card_link/card_link';
import CardInversion from '../modulos/card_inversion/card_inversion';
import CardImpuesto from '../modulos/card_impuesto/card_impuesto'
import SideBar from '../modulos/sideBar/sideBar'
import Profile from '../modulos/profile/profile'
import SideRight from '../modulos/rightSideBar/rightSideBar'

import TimeOut from '../errorTimeOut/errorTimeOut'

export default function Dashboard ()
{
    const [navbarOpen, setNavbarOpen] = useState(true)
    const [rightBar, setRightBar] = useState(true)
    /* const dispatch = useDispatch(); */
    /* const desloguear =() =>{
        dispatch(logout())
    } */

    const handleToggle = () => {
        setNavbarOpen(!navbarOpen)
    }
    
    return(
     
        <div  >
           

        <div id="contenedor"> 
        <div id="header">
        <HeaderComponent />
    </div>
    <div id="profile">
        <Profile/>
    </div>       
    
    <div id="headerHomeMob">
        <div id="header_mobile" onClick={handleToggle}>
            <HeaderHomeMobile/>
        </div>
    </div>
    
    <div id="menu-contenedor">
        <SideBar open={navbarOpen} close={handleToggle} />
    </div>
    <div id="contenedor-margen">
        <div id="contenedor-bienvenida" className="title">Hola Mía</div>
        <div id="cards">      
            <div id="card-Movimientos" className="cardCuentas">
                <div id="contenedor-bienvenida" className="cardLinkMob">Hola Mía</div>
                <CardCuenta/>
                <div className="cardLinkMob"><CardLink/></div>
            </div>
            <div id="card-Movimientos" className="cardLink">
                <CardLink/>
            </div>
            <div id="card-Movimientos" className="cardMovimientos card-Movimientos-Mob">
                <CardMovimiento/>
            </div>                
            <div id="card-Movimientos" className="cardInversion">
                <CardInversion/>
            </div>
            <div id="card-Movimientos" className="cardInversion">
                <CardImpuesto/>
            </div>                
        </div>
        <div id="cardRight">
            <div id="card-Movimientos" className="cardMovimientos">
                <CardMovimiento/>
            </div>
        </div>
    </div>
    
    </div >
        {/* <SideRight/> */}
    </div>
     
    )
}