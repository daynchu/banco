import React,{useState} from "react";
import './desbloquearTco.scss'
import Info from "../../../../assets/iconos/info.svg";
import Close from'../../../../assets/iconos/close.svg'
import {Link} from "react-router-dom";
import HeaderMobile from '../../../modulos/headerMobile/headerMobile'
import { BAJA } from "../typeOperation";
import arrow from '../../../../assets/iconos/Back.svg';

export default function DesbloquearTCO ({open}){
    const [btnDisabled, setbtnDisabled] = useState(false);
    const [borderError, setBorderError] = useState(false);
    const [PreguntasUsuario,SetPreguntasUsuarios] = useState({
        pregunta1: '',
        pregunta2: ''
    })

    const handleChange=(e)=>{
        SetPreguntasUsuarios({
            ...PreguntasUsuario,
            [e.target.name] : e.target.value
        })

        if(PreguntasUsuario.pregunta1 !== '' || PreguntasUsuario.pregunta2 !== ''){
            setbtnDisabled(true);
        }        
    }

    const desbloquearTCO = () => {
        if(PreguntasUsuario.pregunta1 !== '' && PreguntasUsuario.pregunta2 !== ''){
            console.log("Desbloquear");
        }else{
            setBorderError(true);
        }
    }

    return (
        <div>            
            <div id={(open ? 'mainFDesbloquearTCO' : 'mainDesbloquearTCO')}>
            <div id="containerDesbloquearTCO">
                <div id="headerDesbloquearTCO">
                    <div id="backDesbloquearTCO">
                        <Link to={{pathname: '/editTco'}}>
                            <img src={arrow} className="githubIcon" alt="back" />
                        </Link>
                    </div>  
                    <div id="titleDesbloquearTCO">Desbloquear mi tarjeta</div>
                </div>                
                <div id="cardInfoDesbloquear">
                    <div>
                        <div id="cardInfoContainerD">
                            <div><img src={Info} alt="info"/></div>
                            <div>
                                <h5 id="cardInfoTitleD">Info</h5>
                                <h5 id="cardInfoTextD">Si no te acordás de las preguntas de seguridad, podés modificarlas desde acá</h5>
                            </div>
                            <div><img id="cardInfoCloseD" src={Close} alt="close"/></div>
                        </div>                        
                    </div>
                </div>
                <div>
                    <h1 id="subtitleDesbloquearTCO">Por favor, contestá estas preguntas:</h1>
                    <div>
                        <h3>Nombre de tu actriz favorita</h3>
                        <input id={(borderError ? 'error' : 'inputDesbloquearTCO')}
                            name="pregunta1" placeholder="Tu respuesta" onChange={handleChange} />
                    </div>
                    <div>
                        <h3>Banda favorita</h3>
                        <input id={(borderError ? 'error' : 'inputDesbloquearTCO')}
                            name="pregunta2" placeholder="Tu respuesta" onChange={handleChange} />
                    </div>
                </div>
                <div id="textDesbloquearTCO" className="textDesloquearTCOMob">
                    Te recordamos que ciertas operaciones definidas por el Banco Hipotecario, implican el uso de la tarjeta de coordenadas. Al darla de baja, no podrás realizarlas. 
                    <br/><br/>
                    Si estas seguro de que querés dar de baja la tarjeta finalizada en X515 presioná “Continuar”.    
                </div>                
                <div>
                    <div>
                        <button id="buttonGreen" className="btnDesbloquearTCO btnContinueVMob" 
                            disabled={!btnDisabled} onClick={() => {desbloquearTCO()}}>
                            Continuar Desbloquear
                        </button>                
                    </div>
                </div>  
            </div>
            </div>
      </div>    
  );
};