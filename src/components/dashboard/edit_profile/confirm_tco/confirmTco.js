import React from "react";
import './confirmTco.scss'
import Close from'../../../../assets/iconos/close.svg'
import MensajeOk from "../../../../assets/iconos/mensajeOk.svg";
import HeaderMobile from '../../../modulos/headerMobile/headerMobile'

export default function ConfirmTCO (){

  return (
        <div>
            <div id="containerConfirmTco">
                <div id="boxConfirmTco">
                    <div><img id="confirmClose" src={Close} alt="close"/></div>
                    <div id="confirmTco">
                        Ciertas operaciones definidas por el Banco Hipotecario, implican el uso de la tarjeta de coordenadas. Al darla de baja, no podrás realizarlas.
                        <br/><br/>
                        ¿Estas seguro de que querés de dar de baja la tarjeta finalizada en X515?
                    </div>
                    <div>
                        <button id="buttonGreen" className="btnContinueVMob">
                            Si, dar de baja
                        </button>
                    </div>
                </div>  
            </div>
      </div>    
  );
};