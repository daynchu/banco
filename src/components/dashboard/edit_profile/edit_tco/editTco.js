import React,{useState} from "react";
import './editTco.scss'
import FAvanzar from "../../../../assets/iconos/flecha_avanzar.svg"
import {Link} from "react-router-dom";
import HeaderMobile from '../../../modulos/headerMobile/headerMobile'
import BajaTCO from '../baja_tco/bajaTco'
import { BAJA, DESBLOQUEO } from "../typeOperation";
import arrow from '../../../../assets/iconos/Back.svg';

export default function EditarTCO (){
    const [preguntasOpen, setPreguntasOpen] = useState(true)
    const [operacion, setOperacion] = useState()

    const selectBaja = () => {
        setPreguntasOpen(!preguntasOpen)
        setOperacion(BAJA);
    }

    const selectDesbloquear = () => {
        setPreguntasOpen(!preguntasOpen)
        setOperacion(DESBLOQUEO);
    }
      
    return (
        <div>
            <div id="mainEditTCO">
            <div id="containerTCO">
                <div>
                    <div>
                        <div id="headerEditTCO">
                            <div id="backEditTCO">
                                <Link to={{pathname: '/FactoresAutenticacion'}}>
                                    <img src={arrow} className="githubIcon" alt="back" />
                                </Link>
                            </div>                            
                            <div id="titleEditTCO">Factores de autenticación</div>
                        </div>
                    </div>
                </div>
                <div>
                    <h1>Tarjetas de coordenadas</h1>
                </div>
                <div>
                    <div onClick={selectBaja}>
                        <label>
                            <button id="btnFA">Dar de baja mi tarjeta</button>
                            <img src={FAvanzar} alt="flecha" style={{"margin-left": "-2em"}} />
                        </label>
                    </div>
                    <div onClick={selectDesbloquear}>
                        <label>
                            <button id="btnFA">Desbloquear mi tarjeta</button>
                            <img src={FAvanzar} alt="flecha" style={{"margin-left": "-2em"}} />
                        </label>
                    </div>
                </div>                
            </div>
            <BajaTCO open={preguntasOpen}/>
            </div>
      </div>    
  );
};