import React from "react";
import './mensajeOk.scss'
import MensajeOk from "../../../../assets/iconos/mensajeOk.svg";
import HeaderMobile from '../../../modulos/headerMobile/headerMobile'

export default function BajaTCO (){

  return (
        <div>
            <div id="mainMensajeOkFA">
                <div id="containerMensajeOkFA">
                    <div>
                        <img id="iconMensajeOkFA" src={MensajeOk} alt="mensaje"/>
                        <div id="mensajeOkFA">¡Tu tarjeta fue dada de baja con éxito!</div>
                    </div>                
                    <div>
                        <div>
                            <button id="buttonGreen" className="btnMensajeOkFA">
                                Aceptar
                            </button>
                        </div>
                    </div>  
                </div>
            </div>
      </div>    
  );
};