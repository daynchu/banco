import React,{useState} from "react";
import './bajaTco.scss'
import Info from "../../../../assets/iconos/info.svg";
import Close from'../../../../assets/iconos/close.svg'
import {Link} from "react-router-dom";
// import HeaderMobile from '../../../modulos/headerMobile/headerMobile'
// import { BAJA } from "../typeOperation";
import arrow from '../../../../assets/iconos/Back.svg';

export default function BajaTCO ({open}){
    const [btnDisabled, setbtnDisabled] = useState(false);
    const [borderError, setBorderError] = useState(false);
    const [PreguntasUsuario,SetPreguntasUsuarios] = useState({
        pregunta1: '',
        pregunta2: ''
    })

    const handleChange=(e)=>{
        SetPreguntasUsuarios({
            ...PreguntasUsuario,
            [e.target.name] : e.target.value
        })

        if(PreguntasUsuario.pregunta1 !== '' || PreguntasUsuario.pregunta2 !== ''){
            setbtnDisabled(true);
        }        
    }

    const bajaTCO = () => {
        if(PreguntasUsuario.pregunta1 !== '' && PreguntasUsuario.pregunta2 !== ''){
            console.log("Baja");
        }else{
            setBorderError(true);
        }
    }

    return (
        <div>            
            <div id={(open ? 'mainFBajaTCO' : 'mainBajaTCO')}>
            <div id="containerBajaTCO">
                <div>
                    <div>
                        <div id="headerBajaTCO">
                            <div id="backBajaTCO">
                                <Link to={{pathname: '/editTco'}}>
                                    <img src={arrow} className="githubIcon" alt="back" />
                                </Link>
                            </div>                            
                            <div id="titleBajaTCO">Dar de baja mi tarjeta</div>
                        </div>
                    </div>
                </div>
                <div id="cardInfo">
                    <div>
                        <div id="cardInfoContainer">
                            <div><img src={Info} alt="info"/></div>
                            <div>
                                <h5 id="cardInfoTitle">Info</h5>
                                <h5 id="cardInfoText">Si no te acordás de las preguntas de seguridad, podés modificarlas desde acá</h5>
                            </div>
                            <div><img id="cardInfoClose" src={Close} alt="close"/></div>
                        </div>                        
                    </div>
                </div>
                <div>
                    <h1 id="subtitleBajaTCO">Por favor, contestá estas preguntas:</h1>
                    <div>
                        <h3>Nombre de tu actriz favorita</h3>
                        <input id={(borderError ? 'error' : 'inputBajaTCO')}
                            name="pregunta1" placeholder="Tu respuesta" onChange={handleChange} />
                    </div>
                    <div>
                        <h3>Banda favorita</h3>
                        <input id={(borderError ? 'error' : 'inputBajaTCO')}
                            name="pregunta2" placeholder="Tu respuesta" onChange={handleChange} />
                    </div>
                </div>
                <div id="textBajaTCO" className="textBajaTCOMob">
                    Te recordamos que ciertas operaciones definidas por el Banco Hipotecario, implican el uso de la tarjeta de coordenadas. Al darla de baja, no podrás realizarlas. 
                    <br/><br/>
                    Si estas seguro de que querés dar de baja la tarjeta finalizada en X515 presioná “Continuar”.    
                </div>                
                <div>
                    <button id="buttonGreen" className="btnBajaTCO btnContinueVMob" 
                        disabled={!btnDisabled} onClick={() => {bajaTCO()}}>
                        Continuar Baja
                    </button>
                </div>  
            </div>
            </div>
      </div>    
  );
};