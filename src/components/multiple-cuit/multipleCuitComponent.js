import React,{useState} from 'react';
import HeaderComponent from './../modulos/header/header'
// import FooterComponet from './../modulos/footer/footer'
import { useDispatch,useSelector } from "react-redux";
import "./multipleCuitComponent.scss"
import { useMediaQuery } from 'react-responsive'
import { logout,loginMultiple } from "../../actions/auth";
import { Link,Redirect } from 'react-router-dom';
import Scroll from 'react-scroll';
import logo_empresa from "../../assets/iconos/logo_empresa_multiple_cuit.svg"
import FAvanzar from "../../assets/iconos/flecha_avanzar.svg"
import iconSearch from "../../assets/iconos/Icono_search.svg"
import HeaderMobile from '../modulos/headerMobile/headerMobile'



var Element = Scroll.Element;


export default function Mcuits(){

 
    const [filter,setFilter] = useState ("")

    const {user} = useSelector((state) => state.auth )

    // var user= {respuesta:{empresas:[
    //     {cuit:"2313456",razonSocial:"casa"},
    //     {cuit:"53423445",razonSocial:"pepe"},
    //     {cuit:"76553257",razonSocial:"roberto"},
    //     {cuit:"52352352",razonSocial:"cacsa"},
    //     {cuit:"553252345",razonSocial:"perewrpe"},
    //     {cuit:"75525675",razonSocial:"rwetwet"},
    //     {cuit:"9752358",razonSocial:"cadwqdwqsa"},
    //     {cuit:"865235235",razonSocial:"pexzczxpe"},
    //     {cuit:"345325235354",razonSocial:"vcxvxc"},
    //     {cuit:"53535435",razonSocial:"nbjhgvnvb"},
    //     {cuit:"52532342",razonSocial:"jghjgh"},
    //     {cuit:"545325356",razonSocial:"uiyiyu"}
    // ]}}


    // const fruit = ['apple', 'banana', 'orange', 'grapefruit',
    // 'mango', 'strawberry', 'peach', 'apricot'];
    //    const { isLoggedIn } = useSelector((state) => state.auth);
    //    const { message } = useSelector((state) => state.message);
       const dispatch = useDispatch();

    // const [cuits,Setcuits] = useState(null)


    const desloguear =() =>{
        dispatch(logout())
    }


    const enviar = (e) => {
        e.preventDefault();
        console.log(e.target.id)
        // console.log(e.target.value)
        dispatch(loginMultiple(user.documento,user.usuario,user.contaseña,e.target.id))
    }
    
    return(
       
        <div>
             {user === null ? <Redirect to={{pathname: '/Login'}}/>   :
             <div>
             <div id="header_desktop">
                <HeaderComponent />
            </div>
            <div id="header_mobile">
                <HeaderMobile path={'/ValidaMetodos'}/>
            </div>
            <div className="containerformMC">
                <div >
                <h1 className="item1">¿Con qué empresa querés operar?</h1>
                </div>
                <div className="searInput">
                  <input placeholder="Busca tu empresa" type="text" id="inputBlanco" name="filter" value={filter}  onChange={event => setFilter((event.target.value).toUpperCase())}></input>  
                  <img src={iconSearch} alt="iconSearch"  style={{ marginLeft: "-3em" , marginBottom: "-0.5em"}} />
                  
                </div>
            </div>
                <div className="containerList">

                {/* <Element name="test7" className="element" id="containerElement" style={{
          position: 'relative',
          height: '400px',
          overflow: 'scroll',
          marginBottom: '100px',
          isDynamic:true
        }}>

{   (user.respuesta.empresas).filter(f => f.razonSocial.includes(filter) || f.cuit.includes(filter) || filter === '') .map(function(f) {
         return( 
         <div className="botonEmpresa" onClick={enviar} id={f.cuit} >
            <div>
         <img src={logo_empresa} alt="logoEmpresa"  />
         </div>
         <div style={{textAlign: "center"}}>
        
         <span alt="Button"  > {f.razonSocial} <br/> {(f.cuit).slice(0,2)+"-"+(f.cuit).slice(2,(f.cuit).length-1)+"-"+(f.cuit).slice((f.cuit).length-1,(f.cuit).length)}</span>
         </div>
         <div  >
         <img src={FAvanzar} alt="flecha" />
         </div>
         </div>
         )
        })}
        </Element> */}


<Element name="test7" className="element" id="containerElement" style={{
          position: 'relative',
          height: '400px',
          overflow: 'scroll',
          marginBottom: '100px',
          isDynamic:true
        }}>

{   (user.respuesta.empresas).filter(f => f.razonSocial.includes(filter) || f.cuit.includes(filter) || filter === '') .map(function(f) {
         return   <button className="botonEmpresa" value={f.cuit} id={f.cuit} onClick={enviar}> 
         <img src={logo_empresa} id={f.cuit} alt="logoEmpresa"  /> {f.razonSocial} <br/> {(f.cuit).slice(0,2)+"-"+(f.cuit).slice(2,(f.cuit).length-1)+"-"+(f.cuit).slice((f.cuit).length-1,(f.cuit).length)} <img src={FAvanzar} id={f.cuit} alt="flecha" /></button>
        })}
        </Element>
                </div>
            </div>
              } 
        </div>
   
    )
}




