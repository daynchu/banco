import './errorTimeOut.scss'
import React, {useState}  from 'react';
import Close from'../../assets/iconos/close.svg'

export default function Error() {
    const [messageOpen, setmessageOpen] = useState(true)

    const handleToggle = () => {
        setmessageOpen(!messageOpen)
    }

    return(
        <div>
            <div>
                <div id={(messageOpen ? "containerTimeOut" : "closeContainerTimeOut")}>
                    <div id="boxMessageTO">     
                        <div className="alignRight"><img id="imgCloseTO" src={Close} alt="close" onClick={handleToggle}/></div>   
                        <div>
                            <div id="titleTO">Ptss...¿Estás ahí?</div>
                            <div id="textTO">
                                Cerramos tu sesión ya que han pasado 15 minutos de inactividad en tu cuenta. 
                                <br/><br/>
                                Si querés, podés volver a loguearte.
                            </div>
                        </div> 
                        <div>
                            <button id="buttonGreen" className="btnAceptarTO" onClick={handleToggle}>
                                Aceptar
                            </button> 
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    )     
}