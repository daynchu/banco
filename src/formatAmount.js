import React from 'react'

export default function FormatAmount ({number}) {
    return (
            <span>
                {
                    new Intl.NumberFormat('de-DE', {
                        minimumFractionDigits: 2,
                    }).format(number)
                }
            </span>
        
    );   
}